import React from 'react';
import { IconContext } from "react-icons";
import { IoIosArrowBack, IoIosArrowForward } from 'react-icons/io';
import { FaFacebookF, FaTwitter, FaYoutube, FaInstagram, FaVimeoV, FaMediumM } from "react-icons/fa";
// import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import BasicCarousel from '../../components/BasicCarousel/BasicCarousel';
import CenteredCarousel from "../../components/CenteredCarousel/CenteredCarousel";
import FeatureNews from '../../components/FeatureNews/FeatureNews';

import './Home.scss';
import  hside1 from '../../assets/imgs/hside1.jpg'
import black_white2 from '../../assets/imgs/black_white2.jpg'
import TrendingNews from '../../components/TrendingNews/TrendingNews';

const Home = props => {
  const socialIcon = [
    {iconName: FaFacebookF, class: 'facebook'},
    {iconName: FaTwitter, class: 'twitter'},
    {iconName: FaYoutube, class: 'youtube'},
    {iconName: FaInstagram, class: 'insta'},
    {iconName: FaVimeoV, class: 'vimeo'},
    {iconName: FaMediumM, class: 'medium'},
  ]
  return (
    <div className="home">
      <div className="carousel_horizontal">
        <CenteredCarousel/>
      </div>
      <div className="videoNews_block">
          <div className="video-carousel">
            <div className="video-block">
              {/* <video src={video1} width="600" height="300" controls="controls" autoplay="true" /> */}
              <img src={hside1}/>
              <button className="roundPlayBtn"></button>
              <div className="videoDetail">
                <p className="dateTip">TECHNOLOGY / March 26, 2020</p>
                <h3 className="videoHeading">Japan's virus success has puzzled the world. Is its luck running out?</h3>
                <p className="subText">The Property, complete with 30-seat screening from room, a 100-seat amphitheater and a swimming pond and sandy shower...</p>
              </div>
            </div>
            <BasicCarousel/>
          </div>
          <div className="topNews">
            <div className="topNews-btn">
              <button className="whitePrimaryBtn">Related</button>
              <button className="whitePrimaryBtn">Related</button>
              <button className="whitePrimaryBtn">Popular</button>
            </div>
            <div className="topNews_Block">
              {
                [1,2,3,4,5].map((val, index)=>{
                  return (
                    <div className="topNews_row" key= {index}>
                      <div className="topNews_row-img">
                        <img src={black_white2}/>
                      </div>
                      <div className="topNews_row-detail">
                        <p className="detail-topicDate">
                          <span className="topic">
                            Technology 
                          </span>
                            / March 26, 2020
                        </p>
                        <p className="detail-title">
                          Copa America: Luis Suarez from devasted US
                        </p>
                      </div>
                    </div>
                  )
                })
              }
            </div>
          </div>
      </div>
      <div className="featureNews_block marginTopBlock">
        <FeatureNews/>
      </div>
      <div className="trendingAndSocial_block marginTopBlock">
        <div className="trendingNews-container">
          <TrendingNews/>
          <div className="newsrow">
            {
              [1,2,3,4,5,6].map((val, index)=>{
                return (
                  <div className="topNews_row" key= {index}>
                    <div className="topNews_row-img">
                      <img src={black_white2}/>
                    </div>
                    <div className="topNews_row-detail">
                      <p className="detail-topicDate">
                        <span className="topic">
                          Technology 
                        </span>
                          / March 26, 2020
                      </p>
                      <p className="detail-title">
                        Copa America: Luis Suarez from devasted US
                      </p>
                    </div>
                  </div>
                )
              })
            }
          </div>
        </div>
        <div className="socialMostInfo-container">
            <div className="socialCount-block">
              <h2 className="block-title">Follow Us</h2>
              <div className="count-block">
                {
                  socialIcon.map((item, index)=>{
                    return (
                      <a href="#" className={`count-link ${item.class}`} key={index}>
                        <IconContext.Provider value={{ size:'1.5em', color:"#fff" }}>
                            <div style={{padding: '12px'}}>
                                <item.iconName/>
                            </div>
                        </IconContext.Provider>
                        <div className="count-text">
                          <p className="count">34,456</p>
                          <span className="text">Fans</span>
                        </div>
                      </a>
                    )
                  })
                }
              </div>
            </div>
            <div className="mostView-block">
              <div className="title-row">
                  <h2 className="block-title">Most View</h2>
                  <div className="btn-group">
                      <button className="iconbtn">
                      <IconContext.Provider value={{ color: "#808080ad" }}>
                          <IoIosArrowBack/>
                      </IconContext.Provider>
                      </button>
                      <button className="iconbtn">
                          <IconContext.Provider value={{ color: "#808080ad" }}>
                              <IoIosArrowForward/>
                          </IconContext.Provider>
                      </button>
                  </div>
              </div>
              <div className="mostViewRows">
                {
                  [1,2,3,4,5].map((val, index)=>{
                    return (
                      <div className="topNews_row" key= {index}>
                        <div className="topNews_row-img">
                          <img src={black_white2}/>
                        </div>
                        <div className="topNews_row-detail">
                          <p className="detail-topicDate">
                            <span className="topic">
                              Technology 
                            </span>
                              / March 26, 2020
                          </p>
                          <p className="detail-title">
                            Copa America: Luis Suarez from devasted US
                          </p>
                        </div>
                        <div className="countNumber">
                          {index+1}
                        </div>
                      </div>
                    )
                  })
                }
              </div>
            </div>
        </div>
      </div>
    </div>
  );
};

export default Home;