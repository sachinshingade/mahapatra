import React from 'react';
// import { About } from './views/About';
// import { NavBar } from './components/NavBar';
import { Route, Switch, Redirect } from 'react-router-dom';

import Home from './views/Home/Home';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';

export const Routes = () => {
  return (
    <div>
      {/* <NavBar /> */}
      <Header/>
      <Switch>
        <Route exact path="/Home" component={Home} />
        <Route exact path="/">
          <Redirect to="/Home" />
        </Route>
        {/* <Route exact path="/About" component={About} /> */}
      </Switch>
      <Footer/>
    </div>
  );
};