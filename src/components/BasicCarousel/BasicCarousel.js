import React from 'react';
import './BasicCarousel.scss';
import { Carousel } from "react-responsive-carousel";

// Images
import post_gsi1 from '../../assets/imgs/post_gsi1.jpg';
import post_gsi2 from '../../assets/imgs/post_gsi2.jpg';
import post_gsi3 from '../../assets/imgs/post_gsi3.jpg'
import post_gsi4 from '../../assets/imgs/post_gsi4.jpg';
import post_gsi5 from '../../assets/imgs/post_gsi5.jpg';
import post_gsi6 from '../../assets/imgs/post_gsi6.jpg'
import post_gsi7 from '../../assets/imgs/post_gsi7.jpg'


const BasicCarousel = props => {
    const imgArr = [
        {
            imgurl: post_gsi1
        },
        {
            imgurl: post_gsi2
        },
        {
            imgurl: post_gsi3
        },
        {
            imgurl: post_gsi4
        },
        {
            imgurl: post_gsi5
        },
        {
            imgurl: post_gsi6
        },
        {
            imgurl: post_gsi7
        },
        {
            imgurl: post_gsi1
        },
        {
            imgurl: post_gsi2
        },
        {
            imgurl: post_gsi3
        },
        {
            imgurl: post_gsi4
        },
        {
            imgurl: post_gsi5
        },
        {
            imgurl: post_gsi6
        },
        {
            imgurl: post_gsi7
        },{
            imgurl: post_gsi1
        },
        {
            imgurl: post_gsi2
        },
        {
            imgurl: post_gsi3
        },
        {
            imgurl: post_gsi4
        },
        {
            imgurl: post_gsi5
        },
        {
            imgurl: post_gsi6
        },
        {
            imgurl: post_gsi7
        }
    ]
    return(
        <div className="basicCarousel">
            <Carousel
            centerMode
            centerSlidePercentage={10}
            showIndicators={false}
            showStatus={false}
            // selectedItem={0}
            width="100%"
            showThumbs={false}
            showArrows={true}
            infiniteLoop={true}
            autoPlay
            >
                {/* <baseChildren/> */}
                {imgArr.map((item, index)=>{
                    return (
                        <div key={index} style={{ height: "100%", width: 84 }}>
                            <img src={item.imgurl} />
                            {/* <p className="legend">Legend {index}</p> */}
                        </div>
                    )
                })}
            </Carousel>
        </div>
    )
}

export default BasicCarousel;