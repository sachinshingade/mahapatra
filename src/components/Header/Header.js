import React from 'react';
import { IconContext } from "react-icons";
import { IoIosArrowBack, IoIosArrowForward, IoLogoTwitter, IoLogoYoutube, IoLogoInstagram } from 'react-icons/io';
import { FaFacebookF } from "react-icons/fa";
import './Header.scss';


import Nav from '../Nav/Nav';

const Header = props => {
    return(
        <div>
            <div className="header-block">
                <div className="left-block">
                    <div className="trending">Trending</div>
                    <a className="topBest">Top 10 Best Movies of 2018 So Far: Great Movies</a>
                </div>
                <div className="right-block">
                    <div className="btn-group">
                        <button className="iconbtn">
                        <IconContext.Provider value={{ color: "#808080ad" }}>
                            <IoIosArrowBack/>
                        </IconContext.Provider>
                        </button>
                        <button className="iconbtn">
                            <IconContext.Provider value={{ color: "#808080ad" }}>
                                <IoIosArrowForward/>
                            </IconContext.Provider>
                        </button>
                    </div>
                    <div className="dateSocial">
                        <p className="date-show">Thursday, March 26,2020</p>
                        <div className="socialIcon">
                            <IconContext.Provider value={{ color: "#3a4067e0" }}>
                                <div>
                                    <IoLogoTwitter/>
                                </div>
                            </IconContext.Provider>
                            <IconContext.Provider value={{ color: "#3a4067e0" }}>
                                <div>
                                    <FaFacebookF/>
                                </div>
                            </IconContext.Provider>
                            <IconContext.Provider value={{ color: "#3a4067e0" }}>
                                <div>
                                    <IoLogoYoutube/>
                                </div>
                            </IconContext.Provider>
                            <IconContext.Provider value={{ color: "#3a4067e0" }}>
                                <div>
                                    <IoLogoInstagram/>
                                </div>
                            </IconContext.Provider>
                            {/* <IoLogoTwitter/>
                            <ImFacebook/>
                            <IoLogoYoutube/>
                            <IoLogoInstagram/> */}
                        </div>
                    </div>
                </div>
            </div>
            <Nav/>
        </div>
    )
}

export default Header;