import React from 'react';
import './Footer.scss';

const Footer = props => {
    const newsCat = ['Politics', 'Education', 'buisness', 'Obituaries', 'Technology', 'Corrections', 'Science', 'Education', 'Health', "Today's Paper", 'Sports', 'Correction', 'Entertainment', 'Foods']
    
    return(
        <div>
            <div className="footer-block">
                <div className="newsCats-block footer-block__item">
                    <h3 className="block-title">News Categories</h3>
                    <ul className="newsCats_list">
                        {
                            newsCat.map((item, index)=>{
                                return (
                                    <li className="newsCats_list-item" key={index}>
                                        <a href="#">{item}</a>
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>
                <div className="living-block footer-block__item">
                    <h3 className="block-title">News Categories</h3>
                    <ul className="newsCats_list">
                        {
                            newsCat.map((item, index)=>{
                                return (
                                    <li className="newsCats_list-item" key={index}>
                                        <a href="#">{item}</a>
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>
                <div className="moreNews-block footer-block__item">
                    <h3 className="block-title">More News</h3>
                    {
                    [1,2].map((val, index)=>{
                        return (
                        <div className="topNews_row" key= {index}>
                            {/* <div className="topNews_row-img">
                            <img src={black_white2}/>
                            </div> */}
                            <div className="topNews_row-detail">
                            <p className="detail-topicDate">
                                <span className="topic">
                                Technology 
                                </span>
                                / March 26, 2020
                            </p>
                            <p className="detail-title">
                                Copa America: Luis Suarez from devasted US
                            </p>
                            </div>
                            <div className="countNumber">
                            {index+1}
                            </div>
                        </div>
                        )
                    })
                    }
                </div>
            </div>
            <div className="copyrightRow">
                <p>© Copyright {(new Date().getFullYear())}, All Rights Reserved </p>
                <ul className="footer-list">
                    <li className="footer-list-item"><a href="#">About</a></li>
                    <li className="footer-list-item"><a href="#">Advertise</a></li>
                    <li className="footer-list-item"> <a href="#">{`Privacy & policy`}</a></li>
                    <li className="footer-list-item"><a href="#">Contact Us</a></li>
                </ul>
            </div>
        </div>
    )
}

export default Footer;







































// import React from 'react';
// import './Footer.scss';

// const Footer = props => {
//     return(
//         <div className="footer-block">
//             Footer
//         </div>
//     )
// }

// export default Footer;