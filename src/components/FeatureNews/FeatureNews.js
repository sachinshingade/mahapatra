import React from 'react';
import { IconContext } from "react-icons";
import { IoIosArrowBack, IoIosArrowForward } from 'react-icons/io';
import './FeatureNews.scss';
import feature2 from '../../assets/imgs/feature2.jpg';
const FeatureNews = props => {
    return(
        <div className="FeatureNews-block">
            <div className="title-row">
                <h2 className="block-title">Feature News</h2>
                <div className="btn-group">
                    <button className="iconbtn">
                    <IconContext.Provider value={{ color: "#808080ad" }}>
                        <IoIosArrowBack/>
                    </IconContext.Provider>
                    </button>
                    <button className="iconbtn">
                        <IconContext.Provider value={{ color: "#808080ad" }}>
                            <IoIosArrowForward/>
                        </IconContext.Provider>
                    </button>
                </div>
            </div>
            <div className="FeatureNews-block-row">
                {
                    [1,2,3,4].map((item)=>{
                        return (
                            <div className="newsCard" style={{backgroundImage: `url(${feature2})`, backgroundSize: 'cover', width: '22%', height: 350, background: 'linear-gradient(to right, rgba(76,76,76,1) 0%'}}>
                                <div className="newsCard-block">
                                    <p className="dateTip">TECHNOLOGY / March 26, 2020</p>
                                    <p className="title">Best Garden wing supplies for the horticu ltural</p>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default FeatureNews;