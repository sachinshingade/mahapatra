import React from 'react';
import './TrendingNews.scss';
import { IconContext } from "react-icons";
import { IoIosArrowBack, IoIosArrowForward } from 'react-icons/io';
import feature2 from '../../assets/imgs/feature2.jpg';

const TrendingNews = props => {
    return(
        <div className="TrendingNews-block">
            <div className="title-row">
                <h2 className="block-title">Trending News</h2>
                <div className="btn-group">
                    <button className="iconbtn">
                    <IconContext.Provider value={{ color: "#808080ad" }}>
                        <IoIosArrowBack/>
                    </IconContext.Provider>
                    </button>
                    <button className="iconbtn">
                        <IconContext.Provider value={{ color: "#808080ad" }}>
                            <IoIosArrowForward/>
                        </IconContext.Provider>
                    </button>
                </div>
            </div>
            <div className="largeCard-container">
                {
                    [1,2,].map((index, ind)=>{
                        return(
                            <div key={ind} className="largeCard" style={{backgroundImage: `url(${feature2})`, backgroundSize: '100% 50%', width: '48%', height: 450, backgroundRepeat: "no-repeat"}}>
                                <div className="largeCard-detail">
                                    <p className="detail-topicDate">
                                    <span className="topic">
                                        Technology 
                                    </span>
                                        / March 26, 2020
                                    </p>
                                    <h4 className="detail-title">There may be no consoles in the future ea exec says</h4>
                                    <p className="detail-info">
                                        The Property, complete with 30-seat screening from room, a 100-seat amphitheater and a swimming pond with sandy shower...
                                    </p>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default TrendingNews;