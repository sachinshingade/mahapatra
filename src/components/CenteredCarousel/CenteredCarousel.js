import React from 'react';
import './CenteredCarousel.scss';
import { Carousel } from "react-responsive-carousel";
import business1 from '../../assets/imgs/business1.jpg';
import business2 from '../../assets/imgs/business2.jpg';
import business3 from '../../assets/imgs/business3.jpg'

const CenteredCarousel = props => {
    const imgArr = [
        {
            imgurl: business1,
            title: "The home decoration document: photograph of an",
            subText: "People have been infected"
        },
        {
            imgurl: business2,
            title: "The home decoration document: photograph of an",
            subText: "People have been infected"
        },
        {
            imgurl: business3,
            title: "The home decoration document: photograph of an",
            subText: "People have been infected"
        },
        {
            imgurl: business1,
            title: "The home decoration document: photograph of an",
            subText: "People have been infected"
        },
        {
            imgurl: business2,
            title: "The home decoration document: photograph of an",
            subText: "People have been infected"
        },
        {
            imgurl: business3,
            title: "The home decoration document: photograph of an",
            subText: "People have been infected"
        }
    ]
    return(
        <div className="centeredCarousel">
            <Carousel
            centerMode
            centerSlidePercentage={30}
            showIndicators={false}
            showStatus={false}
            // selectedItem={0}
            width="100%"
            showThumbs={false}
            showArrows={true}
            infiniteLoop={true}
            autoPlay
            >
                {imgArr.map((item, index)=>{
                    return (
                        <div key={"slide"+(index+1)} style={{ padding: "0px 10px", height: "100%", width: 400,display: "flex", alignItems: "center" }}>
                            <div className="carouselFlat">
                                <div className="carouselFlat__img">
                                    <img src={ item.imgurl }/>
                                </div>
                                <div className="carouselFlat__detail">
                                    <h3 className="detail_head">{item.title}</h3>
                                    <p className="detail_text">{item.subText}</p>
                                </div>
                            </div>
                        </div>
                    )
                })}
                
                {/* <div key="slide2" style={{ padding: 20, height: 100 }}>
                    Text 02
                </div>
                <div key="slide3" style={{ padding: 20, height: 100 }}>
                    Text 02
                </div>
                <div key="slide4" style={{ padding: 20, height: 100 }}>
                    Text 02
                </div> */}
            </Carousel>
        </div>
    )
}

export default CenteredCarousel;