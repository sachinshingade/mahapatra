import React from 'react';
import { IconContext } from "react-icons";
import { IoIosSearch } from 'react-icons/io';
import { CgProfile } from "react-icons/cg";

import whethImg from '../../assets/imgs/wheth.png'


import './Nav.scss';

const Nav = props => {
    return(
        <nav className="navBar">
            <div className="navBar-left-block">
                <ul className="navItemList">
                    <li className="navItem moreCat" >Home</li>
                    <li className="navItem moreCat">Pages</li>
                    <li className="navItem moreCat">Posts</li>
                    <li className="navItem moreCat">Categories</li>
                    <li className="navItem ">World</li>
                    <li className="navItem">Sports</li>
                    <li className="navItem">Contact</li>
                </ul>
            </div>
            <div className="navBar-right-block">
                <div className="searchProfile divider">
                    <IconContext.Provider value={{ color: "#3a4067e0", size: "1.33em" }}>
                        <div>
                            <IoIosSearch/>
                        </div>
                    </IconContext.Provider>
                    <IconContext.Provider value={{ color: "#3a4067e0", size: "1.3em" }}>
                        <div>
                            <CgProfile/>
                        </div>
                    </IconContext.Provider>
                </div>
                <div className="language divider">
                    <div className="dropDown">
                        <label className="dropDownLabel moreCat">
                            English
                        </label>
                    </div>
                </div>
                <div className="whetherLocation">
                    <img src={whethImg}/>
                    <div className="tempCity">
                        <p className="temp">32*C</p>
                        <p className="cityName">Mumbai</p>
                    </div>
                </div>
            </div>
        </nav>
    )
}

export default Nav;